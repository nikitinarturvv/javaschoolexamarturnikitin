# README #

This is a repo with T-Systems Java School preliminary examination tasks.
Code points where you solution is to be located are marked with TODOs.

The solution is to be written using Java 1.8 only, external libraries are forbidden. 
You can add dependencies with scope "test" if it's needed to write new unit-tests.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : ARTUR NIKITIN
* Codeship : [![Codeship Status for nikitinarturvv/javaschoolexamarturnikitin](https://app.codeship.com/projects/39a25040-1fff-0138-4b9d-0ec84d154a7b/status?branch=master)](https://app.codeship.com/projects/382472)
