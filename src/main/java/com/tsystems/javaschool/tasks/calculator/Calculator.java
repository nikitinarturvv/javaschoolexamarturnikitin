package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
*/

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || !statement.matches("[\\d()+/.*-]*") || statement == "") {
            return null;
        } else
        {
            Calculator calculator = new Calculator();
            ArrayList<String> string = calculator.getStringList(statement);

            string = calculator.getStringWithDouble(string);


            boolean hasBrackets = true;

            try {
                while (hasBrackets) {
                    hasBrackets = false;
                    int count = 0;

                    for (String str :
                            string) {
                        if (str.equals("(")) {
                            count++;
                            hasBrackets = true;
                        } else if (str.equals(")")) {
                            count--;
                        }
                    }
                    if (count != 0) {
                        string = null;
                        break;
                    }
                    string = calculator.GetRidOfBrackets(string);
                }
            } catch (Exception exp) {
                string = null;
            }

            String result = null;
            if (string != null) {
                result = string.get(0);
                double d = Double.parseDouble(string.get(0));
                int i = (int) d;
                if (d % i == 0) {
                    int a = (int) Double.parseDouble(string.get(0));
                    result = String.valueOf(a);
                } else {
                    double dRound = (int)(Math.round(d * 10000))/10000.0;
                    result = String.valueOf(dRound);
                }

                if (string.get(0).equals("Infinity")) {
                    result = null;
                }
            }

            return result;
        }
    }

    public ArrayList<String> getStringList(String str) {
        char[] chars = str.toCharArray();
        ArrayList<String> strings = new ArrayList<String>();
        boolean isNum = true;
        for (int i = 0; i < chars.length; i++) {
            if(isNum) {
                try{
                    int number = Integer.parseInt(String.valueOf(chars[i]));
                    if (strings.size() != 0) {
                        String oldNumber = strings.get(strings.size() - 1);
                        strings.remove(strings.size() - 1);
                        String newNum = oldNumber + chars[i];
                        strings.add(newNum);
                    } else
                        strings.add(String.valueOf(chars[i]));
                } catch (Exception e) {
                    strings.add(String.valueOf(chars[i]));
                    isNum = false;
                }
            } else {
                try{
                    int number = Integer.parseInt(String.valueOf(chars[i]));
                    strings.add(String.valueOf(chars[i]));
                    isNum = true;
                } catch (Exception e) {
                    strings.add(String.valueOf(chars[i]));
                    isNum = false;
                }
            }
        }
        return strings;
    }

    public ArrayList<String> Multiply(ArrayList<String> strings){
        for (int i = 0; i < strings.size(); i++) {
            if(strings.get(i).equals("*")) {
                double result = Double.parseDouble(strings.get(i-1)) * Double.parseDouble(strings.get(i+1));
                strings.remove(i);
                strings.add(i,String.valueOf(result));
                strings.remove(i-1);
                strings.remove(i);
                i--;
            }
        }
        return strings;
    }

    public ArrayList<String> Divide(ArrayList<String> strings){
        for (int i = 0; i < strings.size(); i++) {
            if(strings.get(i).equals("/")) {
                double result = Double.parseDouble(strings.get(i-1)) / Double.parseDouble(strings.get(i+1));
                strings.remove(i);
                strings.add(i,String.valueOf(result));
                strings.remove(i-1);
                strings.remove(i);
                i--;
            }
        }
        return strings;
    }

    public ArrayList<String> Add(ArrayList<String> strings){
        for (int i = 0; i < strings.size(); i++) {
            if(strings.get(i).equals("+")) {
                double result = Double.parseDouble(strings.get(i-1)) + Double.parseDouble(strings.get(i+1));
                strings.remove(i);
                strings.add(i,String.valueOf(result));
                strings.remove(i-1);
                strings.remove(i);
                i--;
            }
        }
        return strings;
    }

    public ArrayList<String> Subtract(ArrayList<String> strings){
        for (int i = 0; i < strings.size(); i++) {
            if(strings.get(i).equals("-")) {
                double result = Double.parseDouble(strings.get(i-1)) - Double.parseDouble(strings.get(i+1));
                strings.remove(i);
                strings.add(i,String.valueOf(result));
                strings.remove(i-1);
                strings.remove(i);
                i--;
            }
        }
        return strings;
    }

    public ArrayList<String> getStringWithDouble(ArrayList<String> strings) {
        boolean isCorrect = true;
        for (int i = 0; i < strings.size(); i++) {
            if(strings.get(i).equals(".")) {
                String result = strings.get(i-1) + strings.get(i) + strings.get(i+1);
                try {
                    double d = Double.parseDouble(result);
                    strings.remove(i);
                    strings.add(i, result);
                    strings.remove(i-1);
                    strings.remove(i);
                    i--;
                } catch (Exception e){
                    isCorrect = false;
                }
            }
        }
        if (isCorrect) {
            return strings;
        } else return null;
    }

    public ArrayList<String> GetRidOfBrackets(ArrayList<String> string) {
        boolean hasBrackets = false;

        for (String str :
                string) {
            if(str.equals("(")) {
                hasBrackets = true;
            }
        }
        Calculator calculator = new Calculator();

        if (!hasBrackets) {
            string = calculator.Divide(string);
            string = calculator.Multiply(string);
            string = calculator.Subtract(string);
            string = calculator.Add(string);

            return string;
        } else {
            for (int i = 0; i < string.size(); i++) {
                int bracketsCount = 0;
                if(string.get(i).equals("(")) {
                    bracketsCount++;
                    for (int j = i+1; j < string.size(); j++) {
                        if(string.get(j).equals(")") && bracketsCount == 1) {
                            ArrayList<String> result = new ArrayList<String>();
                            for (int k = i+1; k < j; k++) {
                                result.add(string.get(k));
                            }
                            result = calculator.GetRidOfBrackets(result);
                            int k  = i;
                            int m = j;
                            while (k <= m)
                            {
                                string.remove(k);
                                m--;
                            }
                            string.add(i,result.get(0));
                        } else if (string.get(j).equals("(")) {
                            bracketsCount += 1;
                        } else if (string.get(j).equals(")") && bracketsCount !=1) {
                            bracketsCount -=1;
                        }
                    }
                }
            }
            return string;
        }
    }


}
