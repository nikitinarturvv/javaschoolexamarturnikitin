package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        PyramidBuilder pyramidBuilder = new PyramidBuilder();
        for (Integer i :
                inputNumbers) {
            if (i == null) {
                throw new CannotBuildPyramidException();
            }
        }
        if (inputNumbers.size() >= Integer.MAX_VALUE-1 ) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int result = inputNumbers.size();

        int[][] pyramid = null;

        if(pyramidBuilder.isListCorrect(result)) {
            int rows = pyramidBuilder.getRows(result);
            int length = pyramidBuilder.getLength(rows);
            pyramid = new int[rows][length];
            pyramid = pyramidBuilder.fillWithZero(pyramid);
            pyramid = pyramidBuilder.makePyramid(inputNumbers, pyramid);
        } else {
            throw new CannotBuildPyramidException();
        }

        return pyramid;
    }

    public boolean isListCorrect(Integer number){
        boolean isCorrect = false;

        double square = (Math.sqrt(number*8 + 1) + 1 )/2;

        int square2 = (int) square;
        if(square % square2 == 0) {
            isCorrect = true;
        }

        return isCorrect;
    }

    public Integer getRows(Integer number) {
        int result = (int) (Math.sqrt(number*8 + 1) + 1 )/2;
        return result-1;
    }

    public Integer getLength(Integer number) {
        return (number - 1) + number;
    }

    public int[][] fillWithZero(int[][] zeroPyramid){
        for (int i = 0; i < zeroPyramid.length; i++) {
            for (int j = 0; j < zeroPyramid[i].length; j++) {
                zeroPyramid[i][j]=0;
            }
        }
        return zeroPyramid;
    }


    public int[][] makePyramid(List<Integer> list, int[][] zeroPyramid){
        int array = list.size() - 1;
        int leftside = 0;
        int rightside = zeroPyramid[0].length-1;
        for (int i = zeroPyramid.length-1; i >= 0; i--) {
            int point = 0;
            for (int j = rightside; j >= leftside; j--) {
                if (point % 2 == 0) {
                    zeroPyramid[i][j] = list.get(array);
                    array--;
                    point++;
                } else {
                    point++;
                }
            }
            leftside++;
            rightside--;
        }

        return zeroPyramid;
    }

}
