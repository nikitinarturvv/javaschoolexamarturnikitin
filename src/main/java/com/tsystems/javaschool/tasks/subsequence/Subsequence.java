package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        boolean result = true;

        Subsequence subsequence = new Subsequence();

        if (x != null && y!= null) {

            List z = new ArrayList();
            for (int i = 0; i < x.size(); i++) {
                z.add(x.get(i));
            }

            for (int i = y.size() - 1; i >= 0; i--) {
                boolean shouldDelete = true;
                for (int j = 0; j < x.size(); j++) {
                    if (y.get(i).equals(x.get(j))) {
                        shouldDelete = false;
                        x.remove(j);
                    }
                }
                if (shouldDelete) {
                    y.remove(i);
                }
            }

            if(y.size() != z.size()) return false;

            for (int i = 0; i < z.size(); i++) {
                if (!z.get(i).equals(y.get(i))) {
                    result = false;
                }
            }
        } else {
            throw new IllegalArgumentException();
        }
        return result;
    }

}
